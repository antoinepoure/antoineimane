#!/bin/bash

#Install nginx
sudo apt update
sudo apt install -y nginx

sudo ufw allow 'Nginx HTTP'

#Install python
sudo apt install -y python3-pip python3-dev build-essential libssl-dev libffi-dev python3-setuptools

sudo apt install -y python3-venv

mkdir /home/vagrant/myproject
cd /home/vagrant/myproject

#Virtual environment
python3 -m venv myprojectenv
source myprojectenv/bin/activate

#Insatll
pip install wheel
pip install gunicorn flask

cp /vagrant/myproject.py /home/vagrant/myproject/

sudo ufw allow 5000

scp /vagrant/wsgi.py /home/vagrant/myproject/
cd /home/vagrant/myproject
gunicorn --bind 0.0.0.0:5000 wsgi:app


